Tris - Complexité
=================

Dans toute la suite,  on se ramènera au cas du tri d'une  liste d'entiers de 0 à
$`n-1`$. Rappelez-vous des danses hongroises disponibles en ligne:

[![](http://img.youtube.com/vi/kDgvnbUIqT4/0.jpg)](http://www.youtube.com/watch?v=kDgvnbUIqT4
"Quick Sort")

Tri sélectif...
----------------

On parcourt la liste, on cherche le plus grand et on l'échange avec
l'élément le plus à droite et on recommence avec la liste privée du plus
grand élément.



On commence par chercher l'indice du maximum d'une liste. On part de 0
et on compare à chaque élément de la liste en faisant évoluer l'indice
du maximum si nécessaire.

```python
    def indMaxi(xs):
        indTmp = 0
        n       = len(xs)
        for i in range(n):
            if xs[i] > xs[indTmp]:
                indTmp = i
        return indTmp
```

Ensuite, on copie la liste donnée en argument pour ne pas l'écraser. On
parcourt la liste et on procède aux échanges éventuels entre le maximum
et l'élément de droite.

```python
    def triSelect(xs):
        cs = xs.copy()
        n  = len(cs)
        for i in range(n - 1,0,-1):
            iM = indMaxi(cs[:i + 1])
            if iM != i:
                cs[iM],cs[i] = cs[i],cs[iM]
            # print(cs) : pour suivre l'évolution
        return cs
```

On va utiliser `permutation` de la bibliothèque `numpy.random` qui renvoie une permutation
uniformément choisie par les permutations de $`\mathfrak{S}_n`$.

```python
    In [5]: ls = list(permutation(range(10)))

    In [6]: ls
    Out[6]: [8, 0, 4, 3, 5, 2, 7, 1, 9, 6]

    In [7]: triSelect(ls)
    [8, 0, 4, 3, 5, 2, 7, 1, 6, 9]
    [6, 0, 4, 3, 5, 2, 7, 1, 8, 9]
    [6, 0, 4, 3, 5, 2, 1, 7, 8, 9]
    [1, 0, 4, 3, 5, 2, 6, 7, 8, 9]
    [1, 0, 4, 3, 2, 5, 6, 7, 8, 9]
    [1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
    [1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
    [1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    Out[7]: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

### Correction

> Étudier la  correction d'un algorithme, c'est  démontrer (ou plus ou  moins se
> convaincre en 1ère...)  que l'algorithme proposé fait ce qu'on  lui demande de
> faire (par exemple ici renvoyer une liste triée).
> À notre niveau, nous  ne pourrons pas vraiment démontrer quoi  que ce soit car
> il nous manque des outils de raisonnement (en particulier le principe de récurrence).

Pour la première fonction, *on peut se convaincre* que `indTemp` est l'indice du
maximum des `i + 1` premiers termes de la liste en argument tout au long de la boucle.

Pour la deuxième fonction, *on peut se convaincre* que la liste des
élements d'indices entre `i` et `n  - 1` est triée.

Donc à la fin de l'exécution de ces fonctions, on a bien ce qui était demandé.

### Complexité

Nous mesurerons la complexité en nombre de comparaisons.

La complexité de  `ind_maxi(xs)` est en $`O(n)`$ avec $`n`$  la longueur de
la liste `xs`.

En effet, il y a une comparaison par itération et $`n`$ itérations.

Pour `tri_select`, on  effectue $`n-1`$ itérations, et  chacune lance `ind_maxi`
sur une liste de longueur $`i+1`$.

Or $`\displaystyle\sum_{i=1}^{n-1}i+1=\sum_{i=2}^ni=(n-2)\frac{2+n}{2}=\frac{n^2-4}{2}`$

donc la complexité est en $`O(n^2)`$.


> Le seul  outil mathématique  un peu  complexe dont nous  aurons besoin  est de
> savoir que la somme des entiers de 1 à n vaut $`\frac{n(n+1)}{2}`$...
>
>


```python
    In [10]: ls = list(permutation(100)) #les entiers de 0 à 99 mélangés

    In [11]: %timeit triSelect(ls)
    1000 loops, best of 3: 561 μs per loop

    In [12]: ls = list(permutation(200)) #les entiers de 0 à 199 mélangés

    In [13]: %timeit triSelect(ls)
    100 loops, best of 3: 2.03 ms per loop

    In [14]: ls = list(permutation(400))

    In [15]: %timeit triSelect(ls)
    100 loops, best of 3: 7.89 ms per loop
```

On observe effectivement que le temps est environ multiplié par 4 quand
la taille de la liste double.


