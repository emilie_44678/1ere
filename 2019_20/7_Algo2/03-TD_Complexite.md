# TD complexité

## 1. Machine de Turing


Nous voudrions construire une machine de  Turing qui calcule le successeur de la
représentation unaire d'un entier.

Une définition d'abord :

**Représentation unaire d'un entier**
*Un nombre  entier $`n\in  N`$ a pour  représentation unaire  $`1^{n+1}`$
c'est-à-dire la concaténation de $`n+1`$ symboles de l'alphabet
$`X=\bigl\{1\bigr\}`$.*

Ainsi, la représentation de  0 est 1, celle de 1 est  11, celle de 2 est
111, etc.

On notera $`\overline{n}`$ la représentation de l'entier $`n`$.

Ainsi, le successeur  de $`n`$ est obtenu en ajoutant un  1 à son écriture
unaire.

La configuration initiale est le mot $`q_0  ☐\overline{n}☐`$.

Le  pointeur rencontre  le premier  blanc et  change donc  d'état  et se
dirige vers la droite jusqu'à rencontrer un deuxième blanc qui est alors
remplacé  par un  1. Le  pointeur change  d'état et  se déplace  vers la
gauche tant qu'il pointe sur un 1.

La  machine s'arrête  lorsqu'elle rencontre  à  nouveau un  blanc car
$`f(q_2, ☐)`$ n'est pas définie.

**Construire une machine de Turing  résolvant le problème en dessinant
  l'automate correspondant.**

Construire  ensuite  une  autre  machine  qui calcule  le  prédécesseur  de  la
représentation unaire d'un entier.



## 2. Vérifier qu'une liste est triée 

On veut écrire une fonction `recherche_trie` qui prend en paramètre une liste
d'entiers et qui renvoie `True` si la liste est correctement triée dans l'ordre
croissant et `False` sinon.

1. Proposer des tests pour cette fonction.
2. Donner le code de cette fonction.
3. Que pensez-vous de sa complexité ?





## 3. Évaluer le nombre de déplacements des valeurs à trier 

1. Reprendre l'algorithme du tri par sélection. Pour une liste de
   $`n`$ valeurs, combien de valeurs seront déplacées dans le pire des cas, pour
   trier toute la liste ?

2. Même question dans le cas du tri par insertion (fiche 32).

3. Comparer les algorithmes du tri par sélection et du tri par insertion en
   terme de nombre de déplacements de valeurs effectués.

## 4. Tri à bulles

Rappel: le tri à bulle fonctionne sur le principe suivant :

* On regarde si les deux premières valeurs sont rangées en ordre croissant. Si
  ce n'est pas le cas, on les échange.
* On regarde si les deux valeurs suivantes sont rangées en ordre croissant. Si
  ce n'est pas le cas, on les échange.
* ...
* Enfin, on regarde si les deux dernières valeurs sont rangées en ordre
  croissant. Si ce n'est pas le cas, on les échange.

Une fois la première «passe» finie, on recommence. Si la liste contient $n$
valeurs, le tableau sera trié au bout de $`n-1`$ passes.

1. Écrire l'algorithme du tri à bulles.
2. À la fin de la première passe, où se trouve la plus grande valeur du tableau?
   Dans ces conditions, la seconde passe doit-elle aller jusqu'à comparer les
   deux dernières valeurs de la liste ? Proposer une version du tri à bulles
   dans laquelle les passes s'arrêtent de plus en plus tôt dans le tableau.
3. Quelle est la complexité du tri à bulles dans la version que vous venez de
   proposer à la question 2 ?









## 5. Recherche dichotomique dans une liste triée

On propose la fonction suivante :

```python
def appartient(liste, element):
    """
    indique si element est dans liste
    liste doit être triée dans l'ordre croissant
    """
    gauche = 0
    droite = len(liste) - 1
    trouve = False
    # AVANT
    while gauche <= droite and not trouve:
        milieu = (gauche + droite) // 2
        # ICI
        if liste[milieu] == element:
            trouve = True
        elif liste[milieu] < element:
            gauche = milieu + 1
        else : # liste[milieu] > element
            droite = milieu - 1
        # LA
    return trouve
```

1. On exécute `appartient([1, 3, 5, 17, 17, 19], 3)`
a. Que renvoie cet appel de fonction ?
b. Le tableau suivant précise les valeurs de certaines variables à chaque tour de boucle. Remplacer les ?? par les valeurs attendues.
c. Pourquoi sort-on de la boucle ?

|       | `gauche` | `milieu` | `droite` | `liste[milieu]` | `trouve` |
| ----- | -------- | -------- | -------- | --------------- | -------- |
| AVANT | 0 | - | 5 | - | False |
| ICI | 0 | 2 | 5 | 5 | ? |
| LA | 0 | 2 | 1 | 5 | ? |
| ICI | 0 | 0 | 1 | 1 | ? |
| LA | 1 | 0 | 1 | 1 | ? |
| ICI | 1 | 1 | 1 | 3 | ? |
| LA | 1 | 1 | 1 | 3 | ? |

2. On exécute `appartient([1, 3, 5, 17, 17, 19], 4)`
a. Que renvoie cet appel de fonction ?
b. Construire un tableau similaire à la question précédente.
c. Pourquoi sort-on de la boucle ?

3. On exécute `appartient([1, 3, 5, 17, 17, 19], 27)`.
a. Que renvoie cet appel de fonction ?
b. Combien de fois va-t-on passer par la ligne `# ICI` ?

4. En vous inspirant de la fonction `appartient`, écrire le code de la fonction suivante :

```python
def indice(liste, element):
    """
    renvoie un indice i tel que liste[i] == element
    s'il y en a un, None sinon.
    """
    # A FAIRE

assert indice([1, 3, 5, 17, 17, 19], 3) == 1
assert indice([1, 3, 5, 17, 17, 19], 4) == None
assert indice([1, 3, 5, 17, 17, 19], 17) == 3 or \
       indice([1, 3, 5, 17, 17, 19], 17) == 4
```















## 6. Décomposition en base 



1.  Donnez la décomposition en binaire (base 2) de l'entier 21.

    On considère la fonction `mystere` suivante:

```python
        def mystere(n, b) :
            """Données : n > 0 un entier et b > 0 un entier
               Résultat : ......."""
            t = [] # tableau vide
            while n > 0 :
                c = n % b
                t.append(c)
                n = n // b
            return t
```

On rappelle que la méthode `append` rajoute un élément en fin de
    liste. Si l'on choisit par exemple `t = [4,5,6]`, alors, après avoir
    exécuté `t.append(12)`, la liste a pour valeur `[4,5,6,12]`.

Pour tout entier naturel non nul *k*, on note $`c_k`$, $`t_k`$ et $`n_k`$ les valeurs
    prises par les variables `c`, `t` et `n` à la sortie de la $`k`$-ème
    itération de la boucle while.

2.  Quelle est la valeur renvoyée lorsqu'on exécute `mystere(256, 10)`?

    On recopiera et complètera le tableau suivant en rajoutant les
    colonnes nécessaires pour tracer entièrement l'exécution:

      | $`k`$  |  1 |  1 |  $`\cdots`$|
      |------- |---| --- |----------|
       |$`c_k`$| | |$`\cdots`$|
       |$`t_k`$| | |$`\cdots`$|
       |$`n_k`$| | |         $`\cdots`$|

3.  Soit $`n>0`$ un entier. On exécute `mystere(n, 10)`. On pose $`n_0=n`$.

    1.  Justifier la terminaison de la boucle `while`.

    2.  On ote $`p`$ le nombre d'itérations lors de l'exécution de `mystere(n, 10)`.
        Justifiez que pour tout $`k\in[\![0,  p]\!]`$, on a $`n_k \leqslant  \frac{n}{10^k}`$.
        Déduisez-en une majoration de *p* en fonction de *n*.

4.  En  vous aidant  du script  de la fonction  `mystere`, écrivez  une fonction
    `somme_chiffres` qui
    prend en argument un entier naturel et renvoie la somme de ses
    chiffres. Par exemple `somme_chiffres(256)`, devra renvoyer 13.

5.  Écrivez une version récursive de la fonction `somme_chiffres` que vous nommerez `somme_rec`.





## 7. Tri sportif

Que pensez-vous de la complexité de cet algorithme de tri écrit en `C++`? 

En `C++`, `i++` signifie en `Python` : `i += 1`.

(source: <http://unclecode.blogspot.tw/2012/02/stupid-sort_2390.html>):

```cpp
void StupidSort()
{
    int i = 0;
    while(i < (size - 1))
    {
           if(data[i] > data[i+1])
          {
                int tmp   = data[i];
                data[i]   = data[i+1];
                data[i+1] = tmp;
                i = 0;
          }
          else
         {
               i++;
         }
     }
}
```


## 8. Mélange de cartes - le mélange de ASF Software

Savoir bien trier c'est aussi savoir bien mélanger.

Afin de convaincre les utilsateurs que leur algorithme de mélange était
juste, la société ASF Software, qui produit les logiciels utilisés par
de nombreux sites de jeu, avait publié cet algorithme pour mélanger des cartes:

```pascal
procedure TDeck.Shuffle;
var
    ctr: Byte;
    tmp: Byte;

    random_number: Byte;
begin
    { Fill the deck with unique cards }
    for ctr := 1 to 52 do
        Card[ctr] := ctr;

    { Generate a new seed based on the system clock }
    randomize;

    { Randomly rearrange each card }
    for ctr := 1 to 52 do begin
        random_number := random(51)+1;
        tmp := card[random_number];
        card[random_number] := card[ctr];
        card[ctr] := tmp;
    end;

    CurrentCard := 1;
    JustShuffled := True;
end;
```

1.  Considérez un jeu de 3 cartes. Dressez l'arbre de tous les mélanges
    possibles en suivant cet algorithme. Que remarquez-vous?

2.  Proposez un algorithme qui corrige ce problème. Dressez l'arbre
    correspondant pour un jeu de trois cartes.

3.  Traduisez la fonction proposée en Python puis votre version corrigée
    en Python.



## 9. Yet Another Sort of Sort


```
POUR i de 1 à n Faire
   POUR j de n à i + 1 par pas de -1 Faire
      SI t[j] < t[j-1]
         Échange t[j] et t[j-1]
      FIΝ SI
   FIΝ POUR
FIN POUR
```


Qu'est-ce que c'est? Qu'est-ce que ça fait? Comment ça marche?
Complexité? En python? Donnez un nom à ce tri.




## 10. Comparaisons de tris et tracés de graphiques


Tracez sur un même graphique les temps d'exécution des tris étudiés en
fonction de la longueur d'une liste mélangée.

Sur un autre graphique, comparez ces tris avec des listes ordonnées dans
le sens croissant, puis avec des listes ordonnées dans le sens
décroissant.

Pour cela, voici quelques rappels sur Matplotlib.

On lance ipython avec l'option `-pylab` ou bien, si vous ne travaillez pas dans
un confortable terminal Unix, vous pouvez toujours importer la chose:

```python
    In [29]: import matplotlib.pyplot as plt

    In [30]: p1 = plt.plot([1,2,3,4,5],[1,2,5,8,3], marker = 'o',label = "Idiot")

    In [31]: p2 = plt.plot([1,2,3,4,5],[3,8,2,6,-1], marker = 'v',label = "Stupide")

    In [32]: plt.legend()
    Out[32]: <matplotlib.legend.Legend at 0x7fbc2c6b76d8>

    In [33]: plt.title("Essai idiot")
    Out[33]: <matplotlib.text.Text at 0x7fbc2babc668>

    In [34]: plt.xlabel("Le temps")
    Out[34]: <matplotlib.text.Text at 0x7fbc2c620898>

    In [35]: plt.ylabel("C'est de l'amour")
    Out[35]: <matplotlib.text.Text at 0x7fbc2baaa2b0>

    In [36]: plt.show()

    In [37]: plt.savefig("zig.pdf")

    In [38]: plt.clf()
```

![image](./IMG/zig.png)

Comment mesurer le temps? On utilise la fonction `perf_counter` de la bibliothèque `time`:

```python
from time import perfCounter

def temps(tri,p):
    debut = perfCounter()
    tri(p)
    return perfCounter() - debut
```
On devrait obtenir:

![image](./IMG/benchmark.png)



## 11. Politique américaine


![image](./IMG/smith.jpg)

Le Sénat américain est composé de deux sénateurs par état (il y a 50
états...). Vous allez récupérer les statistiques sur 46 votes du
109e congrès (2006). Un 1 signifie un Yea, un $`-1`$ un Nay et un 0 une
abstention. Il manque les votes de Jon CORZINE, sénateur
du New-Jersey qui a été victime d'un accident de voiture cette année-là
(cela fait une exception à gérer :-/ ).

On utilisera les fonctions `open, read, split` pour récupérer les données.

1.  Créez un dictionnaire `vs` de type `{Nom : liste des votes}` et un autre, `os`, de type `{Nom : [Parti, État]}`.

    Par exemple:

        In [18]: os['Obama']
        Out[18]: ['D', 'IL']

        In [20]: vs['Obama']
        Out[20]: [1, -1, 1, 1, 1, -1, -1,...]

    On pourra utiliser la fonction `int`et des définitions par compréhension.

2.  Comment utiliser le produit scalaire pour mesurer l'accord ou le
    désaccord entre deux sénateurs?

3.  Écrivez une fonction `compare(sen1, sen2)` qui mesure le degré d'accord de deux sénateurs.
    Par exemple:

        In [21]: compare('Obama', 'McCain')
        Out[21]: 16

4.  Écrivez une fonction `senat_parti(parti)` qui renvoie l'ensemble des noms des sénateurs
    d'un parti donné. Créez aussi une fonction qui renvoie le nom des
    sénateurs par état, le nom des états par parti.

5.  Écrivez une fonction `compare_sen_etat(etat)` qui mesure la cohérence des votes des deux
    sénateurs d'un état donné.

        In [23]: compareEtat('WA')
        Out[23]: (39, 'WA', ('Cantwell', 'Murray'))  

    Classez les états dans l'ordre croissant des cohérences.

6.  Créez une fonction `plus_eloigne(sen, ens)` qui renvoie le nom et la mesure de l'écart du
    sénateur d'un ensemble donné le plus en désaccord avec un sénateur
    donné.

        In [33]: plusEloigne('Obama',senatParti('D'))
        Out[33]: (22, 'Nelson2')

        In [34]: plusEloigne('Obama',senatParti('R'))
        Out[34]: (7, 'Sununu')

    Faites de même avec le plus en accord.

7.  Quel est le parti le plus cohérent? Comment le déterminer à l'aide
    d'une fonction calculant une moyenne de cohérence.

    Par souci d'efficacité, on pourra se souvenir que le produit
    scalaire est distributif sur l'addition des vecteurs.

8.  Écrivez une fonction `moinsDaccord(ens)` qui renvoie le couple de sénateurs les moins
    d'accord d'un ensemble donné:

        In [50]: moinsDaccord(senateurs)
        Out[50]: (('Feingold', 'Inhofe'), -3)  

    Combien de calculs et de comparaisons sont effectués? Pensez-vous
    pouvoir être plus efficace?

9.  Qui est le sénateur le plus Républicain? L'État le plus Démocrate?
    Classez les sénateurs/États selon ces critères.

10. Ouvrez le fichier [`ONU_vote.txt`](ONU_vote.txt) et étudiez-le d'une manière similaire à ce qui
    vient d'être fait.


## 12. MURD3RS (pour les motivé(e)s)


Les américains ont un goût prononcé pour les tueurs en série. Le profil
psychologique de certains de ces héros maléfiques correspond à une
personne logique, froide, méthodique, dangereuse, qui connait bien son
métier, qui travaille suffisamment lentement pour bien faire son boulot
et suffisamment rapidement pour être efficace. Certains sont des
sociopathes, d'autres sont très à l'aise en société : on retrouve
exactement les qualités recherchées chez un programmeur...Bref, si on
apprenait plus de Caml à l'école, les tueurs en série disparaîtraient.
Un policier canadien, Kim ROSSMO, s'est lui aussi
passionné pour les mathématiques et la programmation et devint en 1995
le premier policier canadien à obtenir un doctorat en crimonologie
(<https://en.wikipedia.org/wiki/Kim_Rossmo>). Il a publié un article
(<http://www.popcenter.org/library/crimeprevention/volume_04/10-Rossmo.pdf>)
sur l'analyse géographique des crimes et proposa une formule pour
localiser le lieu d'habitation d'un tueur en série, ou plutôt pour
calculer le lieu le plus probable :

$`p_{i,j} = k \sum_{n=1}^{(\mathrm{nb\; total\;de\;crimes})} \left [ \underbrace{ \frac{\phi_{ij}}{ (|X_i-x_n| + |Y_j-y_n|)^f } }_{ 1^{\mathrm{er}}\mathrm{\;terme} } + \underbrace{ \frac{(1-\phi_{ij})(B^{g-f})}{ (2B - \mid X_i - x_n \mid - \mid Y_j-y_n \mid)^g } }_{ 2^{\mathrm{nd}}\mathrm{\;terme} } \right ],`$

avec

$`\phi_{ij} = \begin{cases} 1, & \mathrm{\quad si\;} ( \mid X_i - x_n \mid + \mid Y_j - y_n \mid ) > B \quad \\ 0, & \mathrm{\quad sinon} \end{cases}`$
Bon, plus synthétiquement, ça peut s'écrire :

$`P(x)=\sum_{\mathrm{lieu       du      crime       c}}\frac{φ}{d(x,c)^f}      +
\frac{(1-φ)B^{g-f}}{(2B-d(x,c))^g }`$ 

....euh...qui est qui là-dedans ? On discrétise un plan, de ville de
préférence, à l'aide d'une grille. On calcule alors la probabilité que
le tueur habite dans la case *x*.

Comme on travaille dans une ville, la distance $`d`$ sera la distance du
taxi de Manhattan :

![image](./IMG/manhattan.png)

ou si vous préférez:

![image](./IMG/pacman.png)

B est le rayon de la Buffer zone  ou zone tampon : c'est un cercle 
(au sens des taxis) autour du lieu du crime. La fonction $`φ`$ est la
fonction caractéristique de cette zone tampon: elle vaut 1 si la cellule
est dans la zone et 0 sinon. $`f`$ et $`g`$ sont des paramètres arbitraires
qui permettent de s'adapter aux données des crimes précédents. On
cherche à présent à dresser une carte des probabilités. On va utiliser
la fonction `pcolormesh` de `matplotlib.pyplot`. Vous pouvez regarder l'aide. Ce qui va motiver notre
construction, c'est la nature des arguments de cette fonction :

```python
    def dessineRossmo( lieux, vrai, B, f, g, tailleX, tailleY) :
       tX = np.arange(tailleX)
       tY = np.arange(tailleY)
       X, Y = np.meshgrid(tX, tY)
       Z = np.vectorize( probaRossmo(lieux, B, f, g) )(X, Y)
       g = cg(lieux)
       plt.pcolormesh( X, Y, Z, cmap = cm.jet)# , shading=’gouraud’)
       plt.plot(vrai[0], vrai[1], ’ks’, mew = 7)
       plt.plot(g[0],g[1], ’ko’, mew = 7)
       plt.colorbar()
       plt.show()
```

Ici, `lieux` est l'ensemble des lieux des crimes, `vrai` est le
véritable domicile du tueur, `taille` et `taille` sont les dimensions de
la grille.

La fonction `cg` calcule les coordonnées du centre de gravité des lieux
des crimes pour comparer avec le candidat de la méthode Rossmo.

`colorbar` permet d'avoir une échelle de couleur pour lire la carte. Il
va falloir bien comprendre la construction de Z... À vous de mener
l'enquête informatique.
ROSSMO fournit des données pour deux tueurs en série
célèbres:

-   le vampire de Sacramento
    (<https://en.wikipedia.org/wiki/Richard_Chase>)

        vampire = {(3, 17), (15, 3), (19, 27), (21, 22), (25, 18)}
        vraiVampire = ([19],[17])  

-   L'étrangleur de Boston
    (<https://en.wikipedia.org/wiki/Albert_DeSalvo>)

        boston = [[10, 48], [13, 8], [15, 11], [17, 8], [18, 7],
                  [18, 9], [19, 4], [19, 8], [20, 9], [20, 10], [20, 11],
                  [29, 23], [33, 28]]  
        vraiBoston = ([19],[18])

Vous obtiendrez des cartes qui ressemblent à ça :

![image](./IMG/gradientCrime.png)
