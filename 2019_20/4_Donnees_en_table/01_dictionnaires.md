# Dictionnaires



## Caractéristiques

* Ils  existent dans  tous les  langages récents. On  les appelle  parfois `Map`
  (Java, Scala,...).

* Ce sont des conteneurs (comme les ensembles, les listes, les n-uplets).

* Ils sont **mutables**

* Ils sont **non  ordonnés** : l'idée est d'accéder aux éléments  non pas par un
  nombre  (un  index)  par  une   **clé**  choisie  librement  par  la  personne
  programmant.
  
* Pour cette dernière raison, **les clés doivent être non mutables**


## Manipulation

```python
>>> Tel = {}

>>> Tel['Roger'] = '06 12 11 13 20'

>>> Tel
{'Roger': '06 12 11 13 20'}

>>> Tel['Roger']
'06 12 11 13 20'

>>> Tel['Josette'] = '07 00 00 01 00'

>>> Tel['Bill'] = '06 05 04 03 02'

>>> Tel 
{'Bill': '06 05 04 03 02',
 'Josette': '07 00 00 01 00',
 'Roger': '06 12 11 13 20'}

>>> les06 = {ami for ami in Tel if Tel[ami][:2] == '06'}

>>> les06
{'Bill', 'Roger'}

>>> del Tel['Josette']

>>> Tel
{'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20'}

>>> Tel.keys()
dict_keys(['Bill', 'Roger'])

>>> Tel.values()
dict_values(['06 05 04 03 02', '06 12 11 13 20'])

>>> Tel.items()
dict_items([('Bill', '06 05 04 03 02'), ('Roger', '06 12 11 13 20')])
```


## Typage

```python
from typing import Dict

Tel: Dict[str, str] = {'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20'}
```


## Exercices


### Quizzzzz


~~~python
dico = { "a": True, "b": False, "c": False}
~~~

1. Quelle est la valeur de `dico[1]` ?

~~~
☐ a. "a"
☐ b. True
☐ c. "b"
☐ d. False
☐ e. rien car l'expression n'est pas valide
~~~

2. Quelle est la valeur de `dico["a"]` ?

~~~
☐ a. True
☐ b. False
☐ c. rien car l'expression n'est pas valide
~~~

3. Quelle instruction permet de modifier le dictionnaire de façon à ce que sa nouvelle valeur soit 
`{ "a": True, "b": False, "c": False, "e": True}` ?

~~~
☐ a. dico["e"] = True
☐ b. dico.append("e")
☐ c. dico.append("e", True)
☐ d. (7, 4, 4)
☐ e. ce n'est pas possible car un dictionnaire n'est pas modifiable
~~~

4. Quels sont les affichages possibles lors de l'exécution du code

~~~python
for cle in dico.keys():
    print(cle, end=" ")
~~~

~~~
☐ a. a b c
☐ b. (a, True) (b, False) (c, False)
☐ c. True False False
~~~


5. Quels sont les affichages possibles lors de l'exécution du code

~~~python
for truc in dico.items():
    print(truc, end=" ")
~~~

~~~
☐ a. a b c
☐ b. (a, True) (b, False) (c, False)
☐ c. True False False
~~~



### Pokemons


On modélise des informations (nom, taille et poids) sur des pokemons de la façon suivante :

```python
from typing import Dict, Tuple

exemple_pokemons= {
  'Bulbizarre': (2, 7),
  'Herbizarre': (3, 13),
  'Abo': (1, 7),
  'Jungko': (5, 52)}
```

Par exemple, Bulbizarre est un pokémon qui mesure 2 m et qui pèse 7 kg.



1. Quel est le type de `exemple_pokemons` ?

2. Quelle instruction permet d’ajouter à cette structure de données le pokémon Goupix qui mesure 4 m et qui pèse 10 kg ?

3. On donne le code suivant :

```python
def le_plus_grand(pokemons):
    grand = None
    taille_max = None
    for (nom, (taille, poids)) in pokemons.items():
        if taille_max is None or taille > taille_max:
            taille_max = taille
            grand = nom
    return (grand, taille_max)
```
Typer cette fonction en donnant sa signature.

Quelle est la valeur de `le_plus_grand(exemple_pokemons)` ?


4. Écrire le code d’une fonction `le_plus_leger` qui prend des pokemons en paramètre et qui renvoie un tuple 
dont la première composante est le nom du pokemon le plus léger et la deuxième composante est son poids.

```python
assert le_plus_leger(exemple_pokemons) == ('Abo', 7)
```

SOLUTION POSSIBLE :

```python
# LES ACHATS DE MATÉRIEL
from typing import Dict, Tuple

# JE FABRIQUE MES OUTILS
def le_plus_grand(pokemons):
    grand = None
    taille_max = None
    for (nom, (taille, poids)) in pokemons.items():
        if taille_max is None or taille > taille_max:
            taille_max = taille
            grand = nom
    return (grand, taille_max)

def le_plus_leger(pokemons):
    leger = None
    poids_min = None
    for (nom, (taille, poids)) in pokemons.items():
        if poids_min is None or poids < poids_min:
            poids_min = poids
            leger = nom
    return (leger, poids_min)

# JE CONSTRUIS MES PRODUITS
## Je prends la commande
exemple_pokemons = {
  'Bulbizarre': (2, 7),
  'Herbizarre': (3, 13),
  'Abo': (1, 7),
  'Jungko': (5, 52)}

# Je construis en utilisant mes outils
big = le_plus_grand(exemple_pokemons)
light = le_plus_leger(exemple_pokemons)

# je livre
print(f"le plus grand est {big} et le plus léger est {light}")
```


5. Écrire le code d'une fonction `taille` qui prend en paramètre des pokémons ainsi que le nom d'un pokemon, et qui renvoie la taille de ce pokemon.

```python
assert taille(exemple_pokemons, 'Abo') == 13
assert taille(exemple_pokemons, 'Jungko') == 52
assert taille(exemple_pokemons, 'Dracaufeu') is None
```


### Zoo


Au zoo de Beauval, il y a 5 éléphants d'Afrique, 17 écureuils d'Asie, 2 pandas d'Asie ... etc.
On le représente à l'aide d'un dictionnaire, de la façon suivante :

```python
zoo_Beauval: Dict[str, Tuple[str, int]] = {
   'éléphant': ('Asie', 5), 
   'écureuil': ('Asie', 17) , 
   'panda': ('Asie', 2),
   'hippopotame': ('Afrique', 7), 
   'girafe': ('Afrique', 4)}
```

On représente de la même façon le zoo de La Flèche :

```python
zoo_LaFleche: Dict[str, Tuple[str, int]] = {
   'ours': ('Europe', 4), 
   'tigre': ('Asie', 7) ,
   'girafe': ('Afrique', 11),
   'hippopotame': ('Afrique', 3)}
```

1. On souhaite se doter d'une fonction `plus_grand_nombre(zoo)` qui prend un zoo en paramètre et qui renvoie le nom de l'animal le plus représenté dans ce zoo.
Par exemple
```python
assert plus_grand_nombre(zoo_LaFleche) == 'girafe'
assert plus_grand_nombre(zoo_Beauval) == 'écureuil'
```


a. Quelle type de boucle envisagez-vous pour le code de cette fonction ?

~~~
☐ for cle in dico.keys()
☐ for valeur in dico.values()
☐ for (cle,valeur) in dico.items()
☐ je n'utiliserai aucune boucle
~~~

b. Ecrire le code de cette fonction.

2.  On souhaite se doter d'une fonction `nombre_total(zoo, continent)` qui prend un zoo en paramètre ainsi que le nom d'un continent, et qui renvoie le nombre d'animaux originaires de ce continent dans le zoo.
Par exemple :

```python
assert nombre_total(zoo_LaFleche, 'Afrique') == 14
assert nombre_total(zoo_Beauval, 'Asie') == 24
```

# 
a. Quelle type de boucle envisagez-vous pour le code de cette fonction ?

~~~
☐ for cle in dico.keys()
☐ for valeur in dico.values()
☐ for (cle,valeur) in dico.items()
☐ je n'utiliserai aucune boucle
~~~

b. Ecrire le code de cette fonction.

3.  On souhaite se doter d'une fonction `nombre(zoo)` qui prend un zoo en paramètre ainsi que le nom d'un animal, et qui renvoie le nombre de représentants de cet animal dans le zoo.
Par exemple :

```python
assert nombre(zoo_LaFleche, 'panda') == 0
assert nombre(zoo_Beauval, 'panda') == 2
```


a. Quelle type de boucle envisagez-vous pour le code de cette fonction ?

```
☐ for cle in dico.keys()
☐ for valeur in dico.values()
☐ for (cle,valeur) in dico.items()
☐ je n'utiliserai aucune boucle
```

b. Ecrire le code de cette fonction.


### Compteurs littéraires

Créez une fonction `compte(liste: List[str]) -> Dict[str, int]` qui parcourt une
liste de chaînes de  caractères et qui renvoie un dictionnaire  où les clés sont
les différentes  chaînes rencontrées et les  valeurs le nombre de  fois qu'elles
apparaissent dans la liste.

Par exemple:

```python
>>> compteur(['a', 'a', 'b', 'a', 'r', 'g', 'a', 'd', 'd', 'a'])
{'a': 5, 'b': 1, 'r': 1, 'g': 1, 'd': 2}
```


Nous voudrions  appliquer cette fonction  à un  texte littéraire. Pour  cela, on
doit donc récupérer une chaîne, la formater un peu en enlevant les caractères de
ponctuation, les  caractères exotiques,  tout mettre  en minuscules.  Nous avons
déjà vu cela dans un projet précédent. On réutilise aussi `split` et `lower`.


```python
from typing import List
import re

def formate(phrase: str) -> List[str]:
    mots_petits = phrase.lower()
    mots_propres = re.sub('[^A-ZÉÈÀÙÊÔÂÜÛÇÏÎa-zéèàùêôâüûçïî]+', ' ', mots_petits)
    return mots_propres.split()
```


Maintenant, pour récupérer un texte sur internet, rappelons la marche à suivre :

```python
from urllib.request import urlretrieve

urlretrieve('https://www.gutenberg.org/files/2650/2650-0.txt', 'swann.txt')
le_texte = open('swann.txt', 'r').read()
```

Combien de fois le  mot *madeleine* apparaît-il dans *Du côté  de chez Swann* de
Marcel Proust ?

Quels sont les deux mots d'au moins quatre lettres les plus couramment utilisés dans ce texte ? 
