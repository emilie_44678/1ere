# coding: utf-8
#!/usr/bin/env python


#######################################
##
## CALCUL DE DISCRIMINANT
##
#######################################

## IMPORTS

from math import sqrt
import easygui as eg


### ENTRÉES

## version texte
#a = float(input('\nCoefficient de x^2 : '))
#b = float(input('Coefficient de x : '))
#c = float(input('Coefficient constant : '))

## version easygui
champs = ['Coefficient de x^2', 'Coefficient de x', 'Coefficient constant']
msg = 'Entrez les coefficients a, b, c de ax^2 bx + c+'
titre = 'Équation du seconde degré'
a, b, c = eg.multenterbox(msg, titre, champs)
a, b, c = float(a), float(b), float(c)

### VÉRIFICATIONS

assert a != 0, "Ce n'est pas une expression du second degré"

### TRAITEMENT

delta: float = b**2 - 4*a*c
sols = {} if delta < 0 else {(-b - sqrt(delta)) / (2*a), (-b + sqrt(delta)) / (2*a)}
res = f"L'ensemble des solutions est {sols}"

### SORTIE

## version texte
#print(res)

## easygui
eg.msgbox(res, title = "Solutions", ok_button = "Quitter")
