
## Les calories

Ingredients = ['Sa', 'Kz', 'Mo', 'Ke', 'Og']
Cals = [140, 120, 20, 80, 40]
nb_i = len(Ingredients)

no_menu = 1

noms = "\nMenu"
for ing in Ingredients:
    noms += '\t' + ing
noms += "\tCalories"

print(noms)


for k in range(2**nb_i):
    chiffres = bin(k)[2:]
    Ings = [0]*(nb_i - len(chiffres)) + [int(d) for d in chiffres]
    cals = 0
    ligne = str(no_menu)
    for k in range(len(Ings)):
        cals += Ings[k]*Cals[k]
        ligne += f"\t{Ings[k]}"
    print(ligne + '\t' + str(cals))
    no_menu += 1
