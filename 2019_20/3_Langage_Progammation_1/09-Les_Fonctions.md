# Fonctions

![lego](./IMG/lego.jpeg)


Nous  allons   être  amenés  à  construire   des  programmes  de  plus   en  plus
élaborés.  Nous  allons   avoir  besoin  de  les  organiser   en  morceaux  plus
petits. Cela sera plus facile à  contrôler, mais nous permettra aussi d'utiliser
ces blocs élémentaires pour d'autres programmes : c'est la **modularité**.


Une fonction est **déf**inie sur Python en l'introduisant avec le mot-clé `def`.

On **appelle** ensuite la fonction...par son nom.

## Fonction : premier contact


**Déf**inissons par exemple  une fonction qui affiche des  données personnelles sur
une personne :

```python
def afficheMesDonnees():
    print("James")
    print("Bond")
    print("jbond@secret.uk")
```

À présent **appel**ons cette fonction:

```console
>>> afficheMesDonnees()
James
Bond
jbond@secret.uk
```
Une fonction doit être appelée ! 

Quel est l'intérêt ? On aurait pu tout aussi bien écrire :


```python
print("James")
print("Bond")
print("jbond@secret.uk")
```

Oui mais, si  nous avons besoin d'écrire souvent et  dans différents endroits du
code ces adresses, cela  nous permet de ne pas tout réécrire et  en plus, pour la
lecture du code, cela traduit une action.





## Deuxième contact : paramètre et argument


Imaginons  que toute  la famille  Bond  a le  même type  d'adresse e-mail,  seule
l'initiale du prénom changeant. On peut alors proposer le code suivant :


```python
def afficheLesDonneesDe(prenom):
    print(prenom)
    print("Bond")
    print(f"{prenom[0].lower()}bond@secret.uk")
```

et cette fois :

```console
>>> afficheLesDonneesDe("Max")
Max
Bond
mbond@secret.uk

>>> afficheLesDonneesDe("Barbara")
Barbara
Bond
bbond@secret.uk
```

`afficheLesDonneesDe("Max")`  est  le  code **appelant**  (*caller*).  Il  **passe**
l'**argument** (ou **paramètre réel) `Max` à la fonction **appelée** (...par l'appelant).

Quand elle  reçoit l'argument  `"Max"`, la  fonction substitue  le **paramètre**
(ou **paramètre formel**) `prenom` par l'argument `"Max"`.

* un paramètre (formel) est une variable **non-liée** (*unbound*) ;
* un argument (ou paramètre réel) peut être une valeur, une variable liée ou une expression
  complexe.
  
Ainsi, les paramètres ne changent pas d'un  appel à l'autre de la fonction alors
que  les arguments  peuvent changer  (en fait,  ça peut  être plus  compliqué et
subtil mais on se tiendra à cette explication à notre niveau...).


Dans la  pratique, on  peut parfois utiliser  un terme pour  un autre  et parler
d'argument au lieu de paramètre, ou d'argument formel au lieu de paramètre. Bon,
le principal est de bien voir la différence entre les deux notions et on essaiera
donc d'utiliser correctement paramètre et argument.


## Troisième contact : plusieurs arguments

Et que se passe-t-il  si nous voulons aussi changer le nom de  famille et le nom
de domaine ? Il faut passer plusieurs arguments et donc avoir une fonction ayant
plusieurs paramètres...


```python
def afficheLesDonneesDe(prenom, nom, domaine):
    print(prenom)
    print(nom)
    print(f"{prenom[0].lower()}{nom.lower()}@{domaine}")

afficheLesDonneesDe('James', 'Haskell', 'wasps.uk')
```

et on obtient:

```console
$ python fonction_cours.py
James
Haskell
jhaskell@wasps.uk
```

## Quatrième contact : le retour

Tout ceci ne  nous convient pas pour  plusieurs raisons : nous  avions comme mot
d'ordre  de n'avoir  qu'un seul  `print` par  programme pour  matérialiser notre
sortie  finale.  Il faut  donc  abolir  les `print`  et  autres  sorties de  nos
fonctions.

Il faut de plus avoir en tête  qu'une fonction est une machine à transformer des
paramètres d'entrée en paramètres de sortie :

```mermaid
graph LR;
	A{Entrée}-->B[Fonction];
	B-->C{Sortie};
```

Nous préférerons donc la forme suivante qui utilise le mot-clé `return` :

```python
def lesDonneesDe(prenom, nom, domaine):
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}"
    return donnees

print(lesDonneesDe('James', 'Haskell', 'wasps.uk'))
```

Nous pouvons même typer nos fonctions pour faciliter la lecture des programmes :

```python
def lesDonneesDe(prenom: str, nom: str, domaine: str) -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}\n"
    return donnees
	
print(afficheLesDonneesDe('James', 'Haskell', 'wasps.uk'))
```

De manière générale, il faudra présenter les fonctions sous cette forme:


```python
def nomDeLaFonction(param1: type1, param2: type2, ...) -> type_de_la_sortie:
    Traitement des paramètres 
	return valeur_de_sortie
```
 Pour être complet et  si la fonction n'est pas évidente, il  faut prendre l'habitude de
 la documenter à l'aide d'un *docstring*  dans lequel on fera figurer une rapide
 explication  et quelques  tests. Les  *docstrings* sont  entre trois  paires de
 guillemets.
 
 La fonction `afficheLesDonneesDe`  a un nom suffisemment  clair, les paramètres
 sont typés et portent des noms explicites. Un *docstring* serait redondant.


## Cinquième contact : réutilisation

Supposons que nous  disposions d'une liste de triplets  `(prenom, nom, domaine)`
et que nous voulions afficher les données comme d'habitude.

On dispose donc d'une liste du type : 


```python
data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk"), ("Mata", "Hari", "spion.de"), ("Sofia", "Loren", "cinecitta.it")]
```

Plusieurs possibilités s'offrent à nous.  Nous pouvons utiliser la fonction déjà
définie mais faire attention que l'on récupère des triplets quand on parcourt la
liste alors que la fonction attend trois paramètres.

On peut faire :

```python
sortie = ""
for triplet in data:
    prenom, nom, domaine = triplet
    sortie += lesDonneesDe(prenom, nom, domaine)

print(sortie)
```

On peut  créer une fonction  qui traite les listes  de données. Pour  suivre nos
instructions, il  faut pouvoir indiquer que  le paramètre de cette  fonction est
une liste  de triplets  de chaînes  de caractères. Nous  avons besoin  pour cela
d'importer la  bibliothèque `typing` qui  nous permet d'utiliser des  types plus
complexes que les simples `str`, `int`,...
Ici, on va devoir créer des listes  contenant des triplets de chaînes. Ce seront
des `List[Tuple[str, str, str]]`


```python
from typing import List, Tuple

def tasDeDonnees(donnees: List[Tuple[str, str, str]]) -> str:
    """
    donnees est une liste de triplets de la forme (prenom, nom, domaine)
    revoie une chaîne sous la forme :
    prenom
    nom
    pnom@domaine

    >>> data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk")]
    >>> tasDeDonnees(data)
    'James\nHaskell\njhaskell@wasps.uk\nJames\nBond\njbond@secret.uk\n'
    """
    sortie = ""
    for triplet in donnees:
        prenom, nom, domaine = triplet
        sortie += lesDonneesDe(prenom, nom, domaine)
    return sortie

print(tasDeDonnees(data))
```

## Sixième contact : fonctions pures et impures

Une  fonction doit  toujours renvoyer  quelque chose,  même si  cette chose  est
`None`.

Voyons `print` par exemple.

```console
In [6]: type(print(2))
2
Out[6]: NoneType

In [7]: print(2)
2

In [8]: [print(k) for k in range(4)]
0
1
2
3
Out[8]: [None, None, None, None]
```

en fait, `print` est une fonction qui  renvoie `None`. MAIS elle ne fait pas que
ça. Si nous lisons sa documentation:

```console
In [5]: ?print
Docstring:
print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)

Prints the values to a stream, or to sys.stdout by default.
Optional keyword arguments:
file:  a file-like object (stream); defaults to the current sys.stdout.
sep:   string inserted between values, default a space.
end:   string appended after the last value, default a newline.
flush: whether to forcibly flush the stream.
Type:      builtin_function_or_method
```

Elle  a donc  un **effet  secondaire** (*side  effect*) en  dehors de  la valeur
renvoyée. Ce  n'est donc pas  une fonction pure car  elle fait quelque  chose en
dehors de sa signature.

Il faut,  si possible, éviter ce  genre de fonctions car  les effets secondaires
sont  sources  de  nombreux  bugs.   Imaginons  par  exemple  une  variable  qui
représente le score à un jeu durant une partie : 

```python
score: int = 1

def new_score(points: int) -> int:
    global score
    score = score + points
    return score

for parties in range(1,5):
    print(f"\nJ'ai gagné 10 points ! new_score(10) = {new_score(10)}")
```

Ce qui peut préter confusion :



```console
J'ai gagné 10 points ! new_score(10) = 11

J'ai gagné 10 points ! new_score(10) = 21

J'ai gagné 10 points ! new_score(10) = 31

J'ai gagné 10 points ! new_score(10) = 41
```


On remarque que les mêmes appels à `new_score(10)` ne renvoient pas la même
valeur. La variable `score`  est *globale* : elle est en effet  définie en dehors de
la  fonction.  Mais elle  est  modifiée  par cette  fonction  :  c'est un  effet
secondaire de l'appel de cette fonction.

>Une fonction pure est une fonction qui,  quand on lui passe les mêmes arguments,
>renverra toujours la même valeur.
>
>Une fonction impure va changer l'état du système

Il est donc très difficile de contrôler ce genre de fonctions. On peut être
tenté  de les  utiliser  lorsqu'on veut  faire  évoluer un  plateau  de jeu  par
exemple. Mais il faut être conscient des risques.



## Septième contact : paramètre par défaut

Certains  paramètres  d'une fonction  peuvent  avoir  le  plus souvent  la  même
valeur. On leur donne cette valeur par défaut dans la signature de la fonction.

Par  exemple,  reprenons la  fonction  `lesDonneesDe`  mais supposons  que  nous
travaillions pour une entreprise dont tous  les employés ont pour nom de domaine
`mafirme.fr`. Nous  pouvons nous éviter  de toujours  rentrer le nom  de domaine
pour gagner du temps:

```python
def lesDonneesDe(prenom: str, nom: str, domaine: str = "mafirme.fr") -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}"
    return donnees
```

Alors on obtient :

```python
>>> print(lesDonneesDe("Ada", "Lovelace"))
Ada
Lovelace
alovelace@mafirme.fr

>>> print(lesDonneesDe("Charles", "Babbage", "machine.en"))
Charles
Babbage
cbabbage@machine.en
```






## Bilan du vocabulaire

```python
def test(x: int, y: int) -> int:
    somme = 0
    for i in range(y):
        somme = somme + x
    return somme

z = test(5, 4)
```

Dans le script ci dessus :

1. Quel est le nom de la fonction ?
2. Quels sont les paramètres (formels) de la fonction ?
3. Quels sont les arguments (ou paramètres réels) ?
4. Quelles sont les variables locales à la fonction ?
5. Quelle sera la valeur de `z` après l'exécution de ce script ?
6. Quelle est la signature de la fonction ?
7. Est-elle pure ?

## Correction

1. `test`
2. deux paramètres formels : `x` et `y`
3. paramètres réels sont 5 et 4
4. variables locales à la fonction : `somme` et `i`
5. après l'exécution de ce script, `z` vaudra 20.
6. `(x: int, y: int) -> int`
7. Oui : pas d'effets secondaires.


## Structure d'un programme

Un programme va maintenant se présenter en trois parties:

1. Une série d'importations de bibliothèques utilisées en 2.
2. Une série de fonctions qui seront utiles en 3.
3. Un programme  principal qui appellera judicieusement des  fonctions créées en
   2.
   
   
Par exemple ;

```python
#1 Les importations
from typing import List, Tuple

#2 Les fonctions
def lesDonneesDe(prenom: str, nom: str, domaine: str) -> str:
    donnees = f"{prenom}\n{nom}\n"
    donnees += f"{prenom[0].lower()}{nom.lower()}@{domaine}\n"
    return donnees

def tasDeDonnees(donnees: List[Tuple[str, str, str]]) -> str:
    """
    donnees est une liste de triplets de la forme (prenom, nom, domaine)
    revoie une chaîne sous la forme :
    prenom
    nom
    pnom@domaine

    >>> data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk")]
    >>> tasDeDonnees(data)
    'James\nHaskell\njhaskell@wasps.uk\nJames\nBond\njbond@secret.uk\n'
    """
    sortie = ""
    for triplet in donnees:
        prenom, nom, domaine = triplet
        sortie += lesDonneesDe(prenom, nom, domaine)
    return sortie

#3 Le programme principal
## L'entrée
data = [("James", "Haskell", "wasps.uk"), ("James", "Bond", "secret.uk"), ("Mata", "Hari", "spion.de"), ("Sofia", "Loren", "cinecitta.it")]

## Le traitement
res = tasDeDonnees(data)

## La sortie
print(res)
```


