# Programmation événementielle


## Un premier exemple : `onmouseover`

Toujours avec  notre page,  nous voudrions que  la couleur du  corps de  la page
(`body`) change selon que la souris est dessus ou pas.

Cette fois, plaçons le  script en fin de document car le  corps doit être chargé
avant de pouvoir détecter si la souris passe dessus :

```html
.
.
.
 </body>
  <script src="scripts/monalerte.js"></script>
</html>
```

et dans le script `js`:


```javascript
document.body.onmouseover = function () {
    document.body.style.backgroundColor="blue"
};
document.body.onmouseout = function () {
    document.body.style.backgroundColor="orange"
};
```

ou de manière plus décomposée:


```javascript
document.body.onmouseover = function () {sourisOui()};
document.body.onmouseout = function () {sourisNon()};

function sourisOui() {
	document.body.style.backgroundColor="blue";
}

function sourisNon() {
	document.body.style.backgroundColor="orange";
}
```


## Compteur de caractères live

On voudrait compter le nombre de caractères tapés dans un cadre en direct :


```html
<!doctype html>
<html lang="fr">
  <head>
    <title></title>
    <meta charset="utf-8" />
  </head>
  <body>
    <h1>Compteur live</h1>
    <p>Tapez du texte dans le cadre ci-dessous et voyez la magie opérer...</p>
    <textarea id="texte" cols="50" rows="15" onkeyup="frappe()"></textarea>
    <p id="resultat"></p>
  </body>
  <script>
   function frappe() {
       let cpt = document.getElementById("texte").value.length;
       let res = "Nombre de caractères : " + cpt;
       document.getElementById("resultat").innerHTML=res;
   }
  </script>
</html>
```

Quelques nouveautés ici :

*  `<textarea>` permet,  comme  son nom  l'indique, de  réserver  une zône  pour
  rentrer du texte. On identifie cette  balise avec `id` pour pouvoir la repérer
  avec des fonctions `js`.
  
*
  [`onkeyup`](https://developer.mozilla.org/fr/docs/Web/API/GlobalEventHandlers/onkeyup)
  permet de  détecter quand une touche  du clavier est relâchée  après avoir été
  pressée.
  
* `getElenebtById`  repère le premier élément  dans le fichier `html`  qui porte
  l'identifiant mis en argument.
  
*
  [`innerHTML`](https://developer.mozilla.org/fr/docs/Web/API/Element/innertHTML)
  permet de  remplacer la valeur  de lélément repéré et  de le remplacer  par la
  valeur donnée à la suite du `=`.
  
Expliquez alors comment fonctionne ce code.

