# JAVASCRIPT


## Qu'est-ce ?

Créer de belles pages colorées avec  de belles photos de Ste-Pazanne, c'est bien
mais cela reste un peu statique. On veut de la vie, on veut que cela bouge selon
l'heure, les actions  de l'utilisateur,... On veut des images  qui bougent selon
la position de la souris, des animations, du son...

Une troisième couche est donc nécessaire au gâteau que constitue la page web, en
plus de la couche `html` et de la  couche `css`. De plus `html` et `css` ne sont
pas des  langages de programmation mais  de structuration pour le  premier et de
style  pour  le  second.  Traditionnellement on  utilise  Javascript  pour  cela
(surtout parce qu'il y  a une infinité d'outils déjà faits  dans ce langage pour
créer tout ce qu'on peut imaginer ou presque).

Donc quand on charge la page, le `JavaScript` est lancé APRÈS que le `css` et le
`html`  ont été  assemblés. L'endroit  où  on écrira  le script  `js` sera  donc
important. 

`JavaScript` est la version développée  par *Mozilla* du langage ECMAScript créé
en 1995 par Brendan EICH, informaticien  Étatsunien né en 1961. Actuellement, on
en est à la 10e version, publiée en juin 2019.



## Balise `<script>`

On peut écrire directement le script `js` dans la page `html` même si cela n'est
pas une bonne  pratique : il vaut  mieux distinguer les trois  couches du gâteau
dans trois répertoires différents.

Mais   si  le   script   est  unique   et  très   court,   on  peut   l'intégrer
directement. Reprenons notre page `html` de la section précédente et ajoutons une
alerte qui  sera affichée avant le  chargement de la  page. On le met  donc dans
l'en-tête du fichier `html`:

```html
 <head>
    <title>Page émotion</title>
    <meta charset="utf-8">
    <link href="styles/generique.css" rel="stylesheet" type="text/css">
    <script>alert("L'image que vous allez voir peut choquer les citadins !")</script>
  </head>
```
![image pop](./IMG/pop.png)

Vous pouvez remarquer que  la page est en attente de votre clic  sur `OK` et est
donc séparée de la page principale. La fonction `alert` empèche le navigateur de
faire quoi  que ce soit dans  l'onglet considéré tant que  l'utilisateur n'a pas
cliqué sur `OK`.

Dans  le reste  du cours,  nous supposerons  que vous  aurez créé  un répertoire
`./scripts/` dans lequel vous rangerez vos différents scripts `js`.

Dans votre `html` vous aurez cette fois:

```html
 <head>
    <title>Page émotion</title>
    <meta charset="utf-8">
    <link href="styles/generique.css" rel="stylesheet" type="text/css">
    <script src="scripts/monalerte.js"></script>
  </head>
```


Et dans le fichier `./scripts/monalerte.js`:

```javascript
// Cette page crée une alerte pour les âmes sensibles

alert("L'image que vous allez voir peut choquer les citadins !")
```



## Les bases du `JavaScript`


Bon, c'est le moment de découvrir un  nouveau langage en se remémorant nos bases
de `Python` et de `Bash`.

### Les variables

On peut déclarer des variables simplement. Écrivons notre code différemment:

```javascript
// Cette page crée une alerte pour les âmes sensibles
let mes = "L'image que vous allez voir peut choquer les citadins !";
alert(mes);
```

Pour tester  du code `js`  on peut utiliser la  console du navigateur  en tapant
`Ctrl-Shift-J`. 

### Tableaux

```javascript
js> let t = ['a', 'b', 'c'];
js> t[0];
a
js> t[1];
b
js> t.length;
3
js> t.push('AnnE');
4
js> t
a,b,c,AnnE
js> t.sort();
AnnE,a,b,c
js> 'a' < 'A'
false
js> 'A' < 'z'
true
```

### Chaînes de caractères


```javascript
js> let c1 = 'A'
js> let c2 = 'nn'
js> let c3 = 'E'
js> c1 + c2 + c3
AnnE
js> let nom = c1 + c2 + c3
js> 'Bonjour ' + nom
Bonjour AnnE
```


### Instructions/expressions conditionnelles


On retrouve le classique `if...else` sous forme d'instruction :

```javascript
if (age < 18) {
    alert("N'allez pas à Ste-Pazanne")
} else {
    alert("Soyez prudents")
}
```

ou  en  utilisant  une   expression  conditionnelle  avec  l'opérateur  ternaire
`... ? ... : ...` 

```javascript
let message = (age < 18) ? "N'allez à Ste-Pazanne" : "Soyez prudents"
alert(message)
```

Quand on a plus d'une alternative, il est conseillé d'utiliser `switch`:

```javascript
switch(true) {
case (age < 18):
    alert("Ste-Pazanne est trop dangereux pour vous !");
    break;
case(age < 60):
    alert("On ne veut pas de vous à Ste-Pazanne !")
    break;
case(age < 80):
    alert("Vous avez droit à une visite par mois");
    break;
case(age >= 80):
    alert("Bienvenue chez nous !")
}
```

Pour tester ces différentes versions, vous demanderez l'âge de l'utilisateur
avec la fonction `prompt` en faisant précéder les instructions par :

```javascript
let age = prompt("Quel âge avez-vous ?")
```


### Boucles

* On retrouve le `while`:

```javascript
let age_affiche = prompt("Quel est votre âge ?");
let age = parseInt(age_affiche);
let an = 0;
let s = "";
let mes = "";

while (an < 5) {
    an = an + 1;
    age = age + 1;
    s = (an <= 1) ? "" : "s";
    mes = "Dans " + an + " an" + s + " vous aurez " + age + " ans";
    alert(mes);
}
```

Notez ici l'utilisation de de `ParseInt`  qui permet de transformer la chaîne de
caractère stockée avec `prompt` en un entier.

* Le `for`

```javascript
let age_affiche = prompt("Quel est votre âge ?");
let age = parseInt(age_affiche);
let s = "";
let mes = "";

for (an = 1; an <= 5; an++) {
    age++;
    s = (an <= 1) ? "" : "s";
    mes = "Dans " + an + " an" + s + " vous aurez " + age + " ans";
    alert(mes);
}
```

* Le `for...in`

Attention, on  n'obtient que les indices  des tableaux. La variable  `index` ici
vaut successivement 0, 1, 2 et 3...piège !


```javascript
let age_affiche = prompt("Quel est votre âge ?");
let age = parseInt(age_affiche);
let s = "";
let mes = "";
let etapes = [1, 5, 10, 15];
let age_actuel = age;
let an = 0;

for (index in etapes) {
    an = etapes[index];
    age_actuel = age + an;
    s = (an <= 1) ? "" : "s";
    mes = "Dans " + an + " an" + s + " vous aurez " + age_actuel + " ans";
    alert(mes);
}
```

### Fonction

`js` est par de nombreux aspect un langage *fonctionnel*. Nous étudierons ce que
cela veut  dire vraiment l'an prochain  mais disons seulement que  les fonctions
jouent un rôle primordial.

Créer une focntion est simple:

```javascript
function lol(nombre) {
	let rire = "";
	for (cpt = 1; cpt <= nombre; cpt++) {
		rire = rire + "ah ";
	}
	return rire;
}
```


## Typage et conversion de type (coercion)

Voici un célèbre meme sur `js`:

![meme js](./IMG/meme.png)


Vous pouvez vous-même tester sur la console:

![meme console js](./IMG/memeconsole.png)

C'est que `js` pratique "the coercion", i.e. la conversion de type.


![javascript ^^](./IMG/this-is-javascript.jpeg)

Par exemple, dans :

```javascript
0 == "0"
```
`0` est un nombre  alors que `"0"` est une chaîne. Ça ne  devrait pas être égal,
et pourtant...

```javascript
typeof(0)
"number"
typeof("0")
"string"
typeof([])
"object"
```


C'est que `js`, voyant  que `0` est un nombre, convertit ce qui  est à droite en
nombre et `"0"` devient `0` et on a bien `0 == 0`.

Mais `"0" == []` ne marche pas car la conversion de `[]` en chaîne est `""`:

```javascript
[].toString()
""
```

![coercion partout](./IMG/coercion.jpeg)


> IL FAUT DONC ÉVITER LA CONVERSION DE TYPE

Pour cela on doit privilégier par exemple  ici l'opérateur `===` au lieu de `==`
car il empèche les conversions de type:

```javascript
0 === ""
false
0 === "0"
false
0 === []
false
```




