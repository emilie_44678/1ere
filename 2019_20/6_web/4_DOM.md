% DOM

# DOM: Document Object Model

Dans une page HTML, chaque balise (*tag*) est un **noeud** d'un arbre, chaque balise
imbriquée (*nested*) est un **enfant** de ce noeud.
Tout noeud de l'arbre est accessible avec `javascript`.

Reprenons notre première page `html`:

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Page émotion</title>
    <meta charset="utf-8">
  </head>
  <body>
    <h1>Ma première page web</h1>
    <p>Que d'<em>émotion</em> !</p>
    <p>J'ai envie de:</p>
    <ul>
      <li>créer de belles pages</li>
      <li>en écrivant le moins possible</li>
      <li>dans mon hamac</li>
      <li>sous mon arbre</li>
      <li>dans mon jardin</li>
    </ul>

    <p> Pour vous faire rêver pendant le confinement, voici une photo de Ste-Pazanne:</p>
    <p>
      <img src="https://i.ytimg.com/vi/uxmrldXRL_w/hqdefault.jpg" alt="Une belle photo du plus beau village du monde" />
    </p>
  </body>
</html>
```

On voit l'arbre grâce au système d'indentation. On peut le schématiser ainsi:

```mermaid
graph TD;
d(document) --> h(head);
h --> t(title);
h-->meta;
d --> b(body);
b--> ul;
ul --> li1("li");
ul --> li2(li);
ul --> li3(li);
ul --> li4(li);
ul --> li5(li);
p1-->em;
b --> p2(p);
b-->p3(p);
b-->p4(p);
p4-->img;
b -->p1(p);
b-->h1;
```


ou plus simplement:

```mermaid
graph TD;
d(document) --> h(head);
h --> t(title);
h-->meta;
d --> b(body);
b--> ul;
ul --> li;
p-->em;
p-->img;
b -->p;
b-->h1;
```


Maintenant regardons le DOM en action :

![dom1](./IMG/dom1.png)

Nous  avons  tout de  suite  dans  l'onglet 'Styles'  les  éléments  du css  qui
modifieront éventuellement `Que d'émotion !`.
On voit que les enfants héritent de leurs parents (`inherited from html`) 

On peut  changer un  élément de  style en  allant dans  la console.  On remarque
d'abord que l'élément considéré est repéré par un `==$0` grisé sous l'onglet `Elements`.


```html
<em style="color: yellow;">émotion</em> == $0
```

Cela va nous permettre de l'utiliser dans la console (onglet `Console`):

```javascript
> $0.style.color='yellow'
<."yellow"
```
et hop, `émotion` est en jaune.


![dom2](./IMG/dom2.png)


> À FAIRE : mettez `sous mon arbre` en blanc.


On peut même modifier des contenus. Par exemple, toujours dans la console:

```javascript
> document.getElementById('prems')
<.  <li id=​"prems" class=​"important">​créer de belles pages​</li>​
> document.getElementById('prems').innerHTML='Modifier de belles pages'
<. "Modifier de belles pages"
```

Et le texte est modifié en conséquence:

![dom3](./IMG/dom3.png)


> Il existe de nombreuses possibilités ([pour aller plus loin](https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model/Introduction) )
> Vous pouvez également explorer la très pratique bibliothèque Python [webbot](https://webbot.readthedocs.io/en/latest/)

