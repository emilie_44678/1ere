# HTML

## World Wide Web

Nous n'allons pas voir les choses dans  l'ordre. Avant de voir comment le réseau
est organisé, nous allons explorer une de ses applications les plus importantes,
le  WEB.   Ce  n'est   qu'une  partie  des   applications  qui   s'appuient  sur
l'organisation du réseau.  Les pages WEB que nous allons  construire seront pour
la plupart  en *local*, i.e.   à l'intérieur  du réseau constitué  uniquement de
notre propre machine (et éventuellement d'autres machines, imprimantes, mobiles,
tablettes présentes  à la maison nous  en parlerons plus avant  dans le chapitre
consacré au réseau).

Il  faut cependant  dès  à présent  bien distinguer  Internet  et WEB.  Internet
existait bien avant le WEB qui n'en constitue qu'une application.  Outre le WEB,
d'autres applications  d'INternet sont  le courrier élevtronique,  la messagerie
instantannée,  le pair  à  pair,  SSH, etc.  Nous  étudierons  Internet dans  le
prochain chapitre sur le réseau.

Le World  Wide Web est apparu  en 1989 dans  les laboratoires du CERN  en Suisse
sous l'impulsion de l'Anglais Tim Bermers-Lee  et du Belge Robert Cailliau qui y
travaillaient. Ils voulaient faciliter la communication, la collaboration et les
échanges au  sein du  CERN. Son utilisation  à été ensuite  étendue à  la planète
entière.

C'est la  notion d'*hyperlien* qui  est centrale et  qui permet de  passer d'une
ressource à  une autre. La  communication se fait  en suivant un  *protocole* de
communication précis, en  l'occurence HTTP (Hyper Text Transfer  Protocol) et sa
version sécurisée  HTTPS. Une URL (Uniform  Resource Locator) est une  chaîne de
caractères permettant de localiser une ressource du WEB.

Mais ce qui va nous occuper ici  est l'HTML (Hyper Text Markup Language) qui est
le langage privilégié du WEB.



## HTML

### La base

HTML est un langage de **balisage** mais PAS de programmation. C'est un fichier
texte constitué d'**éléments** qui indiquent au navigateur comment structurer la
page.

Voici un élément:

```html
<h1>Ma première page web</h1>
```

Cet élément contient:

* une balise ouvrante :`<h1>`
* un contenu : `Ma première page web`
* une balise fermante : `</h1>`


Toute balise  (*tag* en angliche)  ouvrante doit  avoir une balise  fermante (la
même balise mais avec un `/`). Ici `h1` signifie titre de niveau 1 (*header*).

On peut *imbriquer* les balises :

```html
<p>Que d'<em>émotion</em> !</p>
```

`p` comme *paragraph* et `em` comme *emphasis*.

Il y a 
* les **éléments en ligne** comme `em` ou `strong` (pour le gras) ou `a` (pour
les hyperliens) qui  concernent des contenus de petite taille  sans passage à la
ligne 
* et les **éléments en bloc** comme `p`, `ul` (*Unordered List*)
qui placent le contenu sur un enouvelle ligne.



### À chercher : votre première page WEB



Voici une première page WEB écrite en html :

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>Page émotion</title>
    <meta charset="utf-8">
  </head>
  <body>
    <h1>Ma première page web</h1>
    <p>Que d'<em>émotion</em> !</p>
    <p>J'ai envie de:</p>
    <ul>
      <li>créer de belles pages</li>
      <li>en écrivant le moins possible</li>
      <li>dans mon hamac</li>
      <li>sous mon arbre</li>
      <li>dans mon jardin</li>
    </ul>

    <p> Pour vous faire rêver pendant le confinement, voici une photo de Ste-Pazanne:</p>
    <p>
      <img src="https://i.ytimg.com/vi/uxmrldXRL_w/hqdefault.jpg" alt="Une belle photo du plus beau village du monde" />
    </p>
  </body>
</html>
```
Elle est disponible sur Gitlab [ici](https://gitlab.com/lyceeND/1ere/-/raw/master/2019_20/6_web/CODE/PremierePage.html).

Enregistrez-la quelque part et ouvrez-la depuis votre navigateur (*browser*).

Explorez    les    différentes    balises    utilisées    en    regardant    [la
documentation](https://www.w3schools.com/html/default.asp).

Changez les images, les balises. Créez votre propre page. Entraînez-vous avec le
[quizz](https://www.w3schools.com/html/html_quiz.asp)  (in  angliche  parce  que
vous avez le temps...)

ous pouvez  vérifier si votre  page HTML est conforme  aux normes en  allant sur
[cette page](https://validator.w3.org/nu/#file)

À bientôt pour de nouvelles aventures...


### L'environnement de travail 


Il ressemble à ça : 
![environnement de travail](./IMG/travailHTML.png "env_trav")

Vous avez  d'un côté Visual Studio  et de l'autre votre  navigateur, par exemple
Brave ou Firefox. Il est important de travailler avec un éditeur intelligent qui
facilitera votre frappe.


## CSS 

Tout ceci est fantastique mais manque un peu de style. Dans les temps héroïques,
on notait le style dans le document  `html` : les couleurs, la police, la taille
des caractères, la taille des colonnes, etc.
On a ensuite compris qu'il était primordial de distinguer la *structure*
de la *présentation*.

Les aspects  structurels sont écrits en  `html` tandis que la  présentation sera
dans un fichier séparé et écrite en `css`  : c'est l'évolution qui a eu lieu dans
les années 1990. La version actuelle couremment utilisée est `css3`.

`css` signifie *Cascade  Style Sheet*. Différentes feuilles de  style peuvent en
effet cohabiter et seront organisées en cascade, par exemple:
- selon leur créateur : utilisateur, auteur...
- selon le media : écran, impression, videoprojecteur,...
-  selon l'architecture  du *site*  : un  premier style  générique auquel  vient
  s'ajouter un  style propre à une  rubrique du site, auquel  vient s'ajouter un
  style propre à la langue,...
  
### Premier exemple

Il  se peut  que l'on  crée plusieurs  feuilles de  style. On  va donc  créer un
répertoire `./styles` (`$ mkdir ./styles`) dans lesquels on créera un fichier nommé par exemple
`generique.css`.

Par exemple, on va taper:

```css
h1 {
	color: green;
}
```

Maintenant, il  reste à  expliquer au  fichier `html` où  trouver sa  feuille de
style.  On rajoute  donc  dans l'entête  du fichier  `html`  (entre les  balises
`<head> et </head>`) la ligne suivante:

```html
<link href="styles/generique.css" rel="stylesheet" type="text/css">
```

Enregistrons  et   rafraîchissons...Le  titre  de   la  page  est   vert,  comme
l'environnement de Ste-Pazanne.

Vous voyez même sur votre page VS un petit carré vert de vant `green`...


![titre vert](./IMG/titreVert.png "titre")


Nous venons de créer une **règle `css`**:

* `h1` est le sélecteur;
* `color` est la propriété;
* `green` est la valeur;
* l'ensemble `color: green` est la déclaration.

Le sélecteur étant `h1`, tous les éléments  `<h1>` de la page `html` suivront la
règle émise dans le fichier `css`.


Il  faudra bien  veiller à  mettre l'ensemble  des déclarations  entre `{}`,  de
séparere propriété et valeur par `:` et de finir une déclaration avec `;`.

Il n'y a pas que des sélecteur de type. Par exemple, on voudrait que certains items
d'une liste non ordonnée soit en gras rouge. Rajoutons dans le fichier `html` un
attribut classe aux tags des items que l'on veut mettre en valeur:


```html
<ul>
      <li class="important">créer de belles pages</li>
      <li>en écrivant le moins possible</li>
      <li>dans mon hamac</li>
      <li class="important">sous mon arbre</li>
      <li>dans mon jardin</li>
</ul>
```

puis dans le fichier `css` (n'oubliez  pas d'utiliser les suggestions de VS pour
taper plus vite):

```css
.important {
    font-weight: bolder;
    color: red;
}
```

Maintenant si vous voulez que le premier  item soit en plus en italique, on peut
l'identifier dans le fichier html:

```html
<ul>
      <li id="prems" class="important">créer de belles pages</li>
      <li>en écrivant le moins possible</li>
      <li>dans mon hamac</li>
      <li class="important">sous mon arbre</li>
      <li>dans mon jardin</li>
    </ul>
```

puis dans le `css`:

```css
#prems {
    font-style: italic;
}
```


On peut définir un style global pour toute la page:


```css
html {
    font-size: 15px;
    font-family:  sans-serif;
    background-color: bisque; 
  }
```

et  le changer  pour certains  éléments,  par exemple  la taille  des fontes  et
l'alignement des titres 1:


```css
h1 {
    font-size: 60px;
    color: green;
    text-align: center;
}
```


### les boîtes

Les éléments d'une page `html` sont en fait pour `css` des boîtes rectangulaires
empilées les unes sur les autres. 

On peut régler les espaces entre les éléments dans la boîte et les bords (`padding`), et
entre  les  bords  et  les  autres  boîtes (`margin`).  Il  y  a  normalement  4
dimensions à donner en pixels à chaque fois pour chaque bord (haut, bas, gauche,
droite). En fait on peut définir une ou deux ou trois ou quatre valeurs:

* une seule : la même pour tout le monde;
* deux : une pour haut/bas une pour gauche/droite;
* trois : une pour haut, une pour gauche/droite, une pour bas;
* quatre : haut puis droite puis bas puis gauche (aiguilles d'une montre).

On peut, à la place de la longueur  en `px`, donner un pourcentage ou `auto` qui
centre entre les bords.

Voici notre `css` maintenant:

```css
html {
    font-size: 15px;
    font-family:  sans-serif;
    background-color: bisque; 
  }

  body {
    width: 50%;
    margin: 10px auto;
    background-color: cyan;
    padding: 0 20px 20px 20px;
    border: 5px solid black;
  }

h1 {
    margin: 15px;
    font-size: 60px;
    color: green;
    text-align: center;
}

.important {
    font-weight: bolder;
    color: red;
}

#prems {
    font-style: italic;
}

img {
    display: block;
    margin: 0 auto;
  }
```

et le magnifique résultat:

![résultat final](./IMG/final.png)


### Inspection

Allons sur  ce [site merveilleux](https://www.sainte-pazanne.fr/) et  faisons un
clic droit puis `Inspecter`.

Une Fenêtre s'affiche sur le côté:

![inspection page Ste Pazanne](./IMG/inspecteur.png)


Vous avez sur la colonne de gauche le code `html` et sur la droite le `css`. 

> Parcourez la  structure et  trouvez le titre  `Sainte-Pazanne` et  modifiez sa
> couleur

Repérez `body`, puis `header`, `bottom-header` puis `<h1>Sainte-Pazanne</h1>`

![header](./IMG/header.png)

Vous allez ensuite  surligner la ligne `<h1>Sainte-Pazanne</h1>`  et regarder le
css correspondant:

![header css](./IMG/headercss.png)



Changez alors la couleur de Sainte-Pazanne et sa fonte:

![new header](./IMG/newheader.png)




## Markdown

Pour  écrire  une  page  basique  comme ce  cours,  il  serait  très  fastidieux
d'utiliser  *directement*  `html`  et  `css`.  Au lieu  de  cela,  on  écrit  en
[`markdown`](https://fr.wikipedia.org/wiki/Markdown) qui est un langage à balise
beaucoup plus simple.  Ensuite on peut facilement le code  au format `html`. Par
exemple, ce [code `markdown`](https://gitlab.com/lyceeND/1ere/-/raw/master/2019_20/6_web/1_html.md) est transformé en [ce code `html`](2019_20/6_web/1_html.html)
