from requests import get
from subprocess import call
from webbot import Browser
from time import sleep
import re

login = input("login genumsi ?")
mdp = input("mdp genumsi ?")






themes = ['Représentation de données : Types et valeurs de base (Première) - Autres',
'Représentation de données : Types construits (Première) - Autres',
'Traitement de données en tables (Première) - Autres',
'Interactions entre l’homme et la machine sur le Web (Première) - Autres',
'Architectures matérielles et systèmes d’exploitation (Première) - Autres',
'Langages et programmation (Première) - Autres',
'Algorithmique (Première) - Autres'
]

q = ['20'] #[str(i) for i in range(10,21)] 
t = ['SUJET', 'CORRIGE']

for qq in q:
        tt = t[1]
        file = get(f"https://groupes.renater.fr/sympa/d_read/numerique-sciences-informatiques/Sp%C3%A9cimens%20E3C/Q{qq}-{tt}.docx")
        with open(f"Q{qq}-{tt}.docx","wb") as f:
            f.write(file.content)
        call(['pandoc','-o', f"Q{qq}-{tt}.html", f"Q{qq}-{tt}.docx"])
        rep = []
        with open(f"Q{qq}-{tt}.html","r") as f:
            for line in f:
                line = line.rstrip()
                if line[:7] == "<p>Ques":
                    l = line[-15:-4].strip()
                    for r in l.replace(' ',''):
                        rep.append(r)
        tt = t[0]
        file = get(f"https://groupes.renater.fr/sympa/d_read/numerique-sciences-informatiques/Sp%C3%A9cimens%20E3C/Q{qq}-{tt}.docx")
        with open(f"Q{qq}-{tt}.docx","wb") as f:
            f.write(file.content)
        call(['pandoc','-o', f"Q{qq}-{tt}.md", f"Q{qq}-{tt}.docx"])
        s = ""
        with open(f"Q{qq}-{tt}.md","r+") as f:
            s = f.read()
            #print(s)
            s = s.replace('***\n','\n')
            s = s.replace('**\n','\n')
            s = s.replace('*\n','\n')
            s = s.replace('**Q','## Q')
            s = s.replace('***R','R')
            s = s.replace('**R','R')
            s = s.replace('*R','R')
            s = s.replace('Réponses','## Réponses')
            s = s.replace('*\n','\n')
            s = s.replace('**\\\n','')
            for ind, let in enumerate('A B C D E F G'.split()):
                s = s.replace(f"Thème {let}", f"{ind}")
            for l in ['A ', 'B ', 'C ', 'D ']:
                s = s.replace(l, '### '+ l + '\n')
            #print(s)
        with open(f"Q{qq}-{tt}.md","w") as f:
            f.write(s)
        call(['pandoc',  '--mathjax', '--section-divs', '-o', f"Q{qq}-{tt}.html",  f"Q{qq}-{tt}.md"])
        with open(f"Q{qq}-{tt}.html","r+") as f:
            s = f.read()
            s = re.sub(r"<h\d>.*</h\d>", "", s)
            for l in ['a', 'b', 'c', 'd']:
                s = s.replace(f'id="{l}"', f'id="{l}-0"')
        with open(f"Q{qq}-{tt}.html","w") as f:
            f.write(s)

        web = Browser(False)
        web.go_to(f'file:///tmp/Q{qq}-{tt}.html')
        propositions, questions = [], []
        for i in range(42):
            for cat in 'abcd':
                prop = web.find_elements(id=f"{cat}-{i}")[0].get_attribute('innerHTML')
                propositions.append(prop)
        for cat in 'abcdefg':
            for i in range(1,7):
                ques = web.find_elements(id=f"question-{cat}.{i}")[0].get_attribute('innerHTML')
                questions.append(ques)
        web.quit()


        web = Browser()
        web.go_to('https://genumsi.inria.fr/index.php')
        web.type(login, id='login_g')
        web.type(mdp, id="mdp_g")
        web.click(text="Valider")
        web.click(text="Ajout")

        for nq in range(42):
            web.click(id='lien-fancy')
            web.type(questions[nq],id='inp-question')
            for no, l in enumerate(['A', 'B', 'C', 'D']):
                web.type(propositions[nq*4 + no], id=f"inp-rep{l}")
            web.click(id="bonne_rep_g")
            web.click(id=f"rep_{rep[nq]}_g")
            web.click(text='Choisir un domaine...')
            web.click(text=themes[nq//6])
            web.click(id="rendu_g")
            web.click(id="insertion_g")
            web.click(text="Nouvel Ajout")
            print(f"Question {nq} OK")

        print(f"Sujet {qq} OK")
        web.quit()
